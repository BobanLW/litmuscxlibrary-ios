//
//  ViewController.swift
//  LitmusCXLibrary
//
//  Created by Fenil15 on 03/28/2017.
//  Copyright (c) 2017 Fenil15. All rights reserved.
//

import UIKit
import LitmusCXLibrary

class ViewController: UIViewController {

    @IBOutlet weak var copySwitch: UISwitch!
    @IBOutlet weak var shareSwitch: UISwitch!
    @IBOutlet weak var dialogSwitch: UISwitch!
    @IBOutlet weak var moreImageSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func fnOpenLitmusFeedback(_ sender: Any) {
        //Edit below details 
        let strAppId = "xxxxxxx" // to be replaced
        let nReminderNumber = -1 // -1, if get link from server else actual number which will get save link from User defaults if available for same app id and reminder number
        let strUserId = "xxxxxxx" // to be replaced
        let strUserName = "xxxxxxx" // to be replaced
        let strUserEmail = "xxxxxxx" // to be replaced
        let isAllowMultipleFeedbacks = true // false, if only one feedback per user id
        
        // Getting user preferences
        let isCopyEnabled = copySwitch.isOn
        let isShareEnabled = shareSwitch.isOn
        let isDialogEnabled = dialogSwitch.isOn
        let isMoreImageBlack = moreImageSwitch.isOn
        
        
        //tagParameters : [String: AnyObject]
        if isDialogEnabled {
            LitmusCXDialogView.fnOpenLitmusFeedback(nil, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: nil, isMoreImageBlackElseWhite: isMoreImageBlack, isCopyEnabled: isCopyEnabled, isShareEnabled: isShareEnabled, viewController: self)
            
        } else {
            
            LitmusCXViewController.fnOpenLitmusFeedback(nil, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: nil, isMoreImageBlackElseWhite: isMoreImageBlack, isCopyEnabled: isCopyEnabled, isShareEnabled: isShareEnabled, viewController: self)
        }
        
    }
}

