/* Copyright (C) Litmusworld Pvt Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Kapil Dave <kapil@litmusworld.com>, August 2016
 */


import UIKit


open class LitmusViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var litmusWebView: UIWebView!
    
    @IBOutlet weak var closeButton: UIButton!
    
    open var notificationPayLoad : AnyObject?
    
    var strBaseUrl : String?
    
    var window: UIWindow?
    
    let defaults = UserDefaults.standard
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        if notificationPayLoad != nil {
            //Set default values received from the notification.
            makeDefault(notificationPayLoad!)
            
            if notificationPayLoad!["base_url"] != nil {
                strBaseUrl = notificationPayLoad!["base_url"] as? String
            }
            
        }
        
        setUpLitmusRater()
    }
    
    public static func fnOpenLitmusFeedback(_ strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, viewController : UIViewController) {
        
        fnOpenLitmusFeedback(nil, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, viewController: viewController);
        
    }
    
    public static func fnOpenLitmusFeedback(_ strBaseUrl : String?, strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, viewController : UIViewController) {
        
        
        let notificationPayLoad = LitmusCXView.fnCreateNotificationPayload(strBaseUrl,
            strAppId : strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: nil, isMoreImageBlackElseWhite: true, isCopyEnabled: false, isShareEnabled: false)
        
        //Load the litmusView Controller with web view.
        
        let podBundle = Bundle(for: LitmusViewController.classForCoder())
        let bundleURL = podBundle.url(forResource: "LitmusCXLibrary", withExtension: "bundle")
        let frameWorkBundle = Bundle(url: bundleURL!)
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Litmus", bundle: frameWorkBundle)
        let litmusViewController : LitmusViewController = mainStoryboard.instantiateViewController(withIdentifier: "litmusView") as! LitmusViewController
        
        litmusViewController.notificationPayLoad = notificationPayLoad as AnyObject?
        
        viewController.present(litmusViewController, animated: false, completion: nil)
    }
    
    
    
    
    //Persist data some where to get it later when needed.
    func makeDefault(_ notificationPayLoad : AnyObject)
    {
        let appId =  notificationPayLoad["app_id"];
        let reminderNumber =  notificationPayLoad["reminder_number"];
        let userEmail =  notificationPayLoad["user_email"];
        let customerId =  notificationPayLoad["customer_id"];
        
        defaults.set(appId, forKey: "notification_appId")
        defaults.set(reminderNumber, forKey: "notification_reminderNumber")
        defaults.set(userEmail, forKey: "userEmail")
        defaults.set(customerId, forKey: "customerId")
    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpLitmusRater(){
        
        //Once the view is loaded. Show loading icon.
        displayLitmusLoadingPage()
        //After displaying loading. Get link and redirect.
        getLitmusLinkAndOpen()
    }
    
    //Load status HTML page for loading icon.
    func displayLitmusLoadingPage(){
        //Get local html file
        
        let podBundle = Bundle(for: self.classForCoder)
        let bundleURL = podBundle.url(forResource: "LitmusCXLibrary", withExtension: "bundle")
        let frameWorkBundle = Bundle(url: bundleURL!)
        
        let localfilePath = frameWorkBundle?.url(forResource: "litmus", withExtension: "html");
        //Create request for URL.
        let myRequest = URLRequest(url: localfilePath!);
        //Define a tag so web view can be closed if needed.
        litmusWebView.tag = ConfigurationConstant.WEB_VIEW_TAG;
        //Load static page on web view.
        litmusWebView.loadRequest(myRequest);
    }
    
    //Function to call and get the URL from Litmus Server and Change the webview URL.
    func getLitmusLinkAndOpen()
    {
        //Get appId & reminderNumber from the cache if available.
        let notificationAppId = defaults.object(forKey: "notification_appId") as! String?
        let notificationReminderNumber = defaults.object(forKey: "notification_reminderNumber") as! NSNumber?
        
        let isAllowMultipleFeedbacks = self.notificationPayLoad!["allow_multiple_feedbacks"] as! Bool
        let strCustomerName = self.notificationPayLoad!["name"] as! String
        
        
        //Get Email from cache
        let userEmail = defaults.object(forKey: "userEmail") as! String?
        
        //If only appId and emailId is availble.
        if(notificationAppId != nil && notificationReminderNumber != nil){
            //Check if we have longUrl for same appId and reminderNumber
            let appId = defaults.object(forKey: "appId") as! String?
            let reminderNumber = defaults.object(forKey: "reminderNumber") as! NSNumber?
            let longUrl = defaults.object(forKey: "longUrl") as! String?
            let customerId = defaults.object(forKey: "customerId") as! String?
            
            if(appId != nil && reminderNumber != nil && longUrl != nil && reminderNumber != -1) {
                if(appId == notificationAppId && reminderNumber == notificationReminderNumber){
                    if(longUrl != nil){
                        openWebViewWithUrl(longUrl!)
                    }
                    else{
                        generateURL(notificationAppId, userEmail: userEmail, reminderNumber : notificationReminderNumber, customerId: customerId, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strCustomerName)
                    }
                }
                else{
                    generateURL(notificationAppId, userEmail: userEmail, reminderNumber : notificationReminderNumber, customerId: customerId, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strCustomerName)
                }
            } else{
                generateURL(notificationAppId, userEmail: userEmail, reminderNumber : notificationReminderNumber, customerId: customerId, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strCustomerName)
            }
        }
    }
    
    
    //Generate URL based on appId && userEmail
    func generateURL(_ appId : String!, userEmail : String!, reminderNumber : NSNumber!, customerId : String!, isAllowMultipleFeedbacks : Bool, strUserName : String){
        
        let restAPIManger = RestApiManager()
        
        //Call API and get Long URL.
        restAPIManger.getLitmusURL(strBaseUrl, appId: appId!, notifyChannel: ConfigurationConstant.NOTIFY_CHANNEL, userEmail: userEmail! , customerId :customerId!, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strUserName, oTagParameters: nil) { (long_url : String) in
            if ((long_url) == "No URL") {
                //If URL is not available Or Some Error. Close web view.
                self.view.viewWithTag(55)?.removeFromSuperview()
            }else {
                //Once Long URL is generated. Push appId, remidernumber and long_url to cache. So that it can be used later.
                self.defaults.set(long_url, forKey: "longUrl")
                self.defaults.set(reminderNumber, forKey: "reminderNumber")
                self.defaults.set(appId, forKey: "appId")
                
                self.openWebViewWithUrl(long_url);
            }
        }
    }
    
    
    //Open link in web view.
    func openWebViewWithUrl(_ long_url : String){
        self.litmusWebView.delegate = self
        
        let url = URL (string: long_url);
        let requestObj = URLRequest(url: url!);
        self.litmusWebView.loadRequest(requestObj);
    }
    
    
    @IBAction func closeButtonClicked(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }
    
    open func webViewDidFinishLoad(_ webView : UIWebView) {
        
        //        print(webView.request!.URL!.absoluteString)
        
        self.closeButton.setTitleColor(UIColor.white, for: UIControl.State())
    }
    
    
    //    @IBAction public func closeLitmusRating(segue:UIStoryboardSegue) {
    //        // Additional Action for unwind segue
    //        let a = "A"
    //
    //    }
    
    
    
    
}

