//
//  LitmusMoreOptionsViewController.swift
//  LitmusCXLibrary
//
//  Created by admin on 18/12/17.
//

import UIKit

class LitmusMoreOptionsViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    var isShareEnabled : Bool = false
    var isCopyEnabled : Bool = false
    var delegate : LitmusMoreOptionsDelegate?
    
    public static func fnGetMoreOptionsViewController(_ isShareEnabled : Bool, isCopyEnabled : Bool, delegate : LitmusMoreOptionsDelegate?) -> LitmusMoreOptionsViewController {
        
        let litmusMoreOptionsViewController : LitmusMoreOptionsViewController = LitmusMoreOptionsViewController()
        
        litmusMoreOptionsViewController.isShareEnabled = isShareEnabled
        litmusMoreOptionsViewController.isCopyEnabled = isCopyEnabled
        litmusMoreOptionsViewController.delegate = delegate
        
        return litmusMoreOptionsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.preferredContentSize = LitmusMoreOptionsViewController.fnGetFrameSize(isShareEnabled, isCopyEnabled: isCopyEnabled)
        // Do any additional setup after loading the view.
        
        let litmusMoreOptionsView = LitmusMoreOptionsView.fnGetMoreOptionsView(self.isShareEnabled, isCopyEnabled: self.isCopyEnabled, delegate: self.delegate)
        
        litmusMoreOptionsView.frame = self.view.frame
        
        self.view.addSubview(litmusMoreOptionsView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Calculate Frame Size
    
    public static func fnGetFrameSize(_ isShareEnabled : Bool, isCopyEnabled : Bool) -> CGSize {
        
        let fWidth : CGFloat = 120
        let fDefaultSingleCellHeight : CGFloat = 44.0
        
        var nNumberOfCells : Int = 1
        
        if isShareEnabled {
            nNumberOfCells = nNumberOfCells + 1
        }
        
        if isCopyEnabled {
            nNumberOfCells = nNumberOfCells + 1
        }
        
        let fHeight : CGFloat = CGFloat(nNumberOfCells) * fDefaultSingleCellHeight
        
        return CGSize( width: fWidth,height: fHeight)
    }
    
    // MARK: - Show view controller as popup
    
    open func showPopover(base: UIView) {
        let viewController = self
        viewController.modalPresentationStyle = .popover
        viewController.modalTransitionStyle = .coverVertical
        
        if let pctrl = viewController.popoverPresentationController {
            //pctrl.delegate = self
            
            pctrl.sourceView = base
            pctrl.sourceRect = base.bounds
//            pctrl.sourceRect = CGRect(x:0,y:0,width:frameSize.width,height:frameSize.height)
            pctrl.delegate = self
            
            // only show upward arrow irrespective of size
            pctrl.permittedArrowDirections = .up

            var currentViewController = LitmusCXView.getCurrentViewController(base)
            
            if currentViewController == nil {
                currentViewController = LitmusCXView.topMostController()
            }
            
            if currentViewController != nil {
                
//                let litmusKLCPopUpView = LitmusCXView.getCurrentLitmusKLCPopUpView(base)
//
//                if litmusKLCPopUpView != nil {
//
//                    currentViewController!.view.sendSubview(toBack: litmusKLCPopUpView!)
//
////                    litmusKLCPopUpView?.bringSubview(toFront: <#T##UIView#>)
//                }
                
                currentViewController!.present(viewController, animated: true, completion: nil)
//                currentViewController!.show(viewController, sender: nil)
            }
        }
    }
    
    
    // MARK: - UIPopoverPresentationControllerDelegate method
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
