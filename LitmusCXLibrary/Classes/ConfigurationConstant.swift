/* Copyright (C) Litmusworld Pvt Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Kapil Dave <kapil@litmusworld.com>, August 2016
 */

import Foundation

/*
    Configuration file for the server URL.
    It contains Server Addresse, API address and URL address to server APIs
*/
struct ConfigurationConstant {
    
    //Server Address
    static let SERVER_ADDRESS = "https://app.litmusworld.com/rateus/";
    
    //API Prefix
    static let API_PREFIX = "api/feedbackrequests/";
    
    // 
    static let GENERATE_FEEDBACK_URL = API_PREFIX + "generate_customer_feedback_url"
    // "generate_customer_feedback_url_2" -> previously using this url
    
    //API urls
    static let GENERATE_URL_REQUEST = SERVER_ADDRESS + GENERATE_FEEDBACK_URL
    
    
    static let NOTIFY_CHANNEL = "none";
    static let APP_ID_FIELD = "app_id";
    static let NOTIFICATION_FIELD = "notify_channel";
    static let EMAIL_ID_FIELD = "user_email";
    static let CUSTOMER_ID_FIELD = "customer_id";
    static let ALLOW_MULTIPLE_FEEDBACK_FIELD = "allow_multiple_feedbacks";
    static let NAME_FIELD = "name";
    static let TAG_OS_FIELD = "tag_os";
    static let TAG_USERID_FIELD = "tag_user_id";
    
    static let WEB_VIEW_TAG = 55;
}

