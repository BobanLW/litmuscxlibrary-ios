# LitmusCXLibrary


[comment]: # (build status)
[comment]: # (     [![CI Status](http://img.shields.io/travis/Fenil15/LitmusCXLibrary.svg?style=flat)](https://travis-ci.org/Fenil15/LitmusCXLibrary)     )
[![Version](https://img.shields.io/cocoapods/v/LitmusCXLibrary.svg?style=flat)](http://cocoapods.org/pods/LitmusCXLibrary)
[![License](https://img.shields.io/cocoapods/l/LitmusCXLibrary.svg?style=flat)](http://cocoapods.org/pods/LitmusCXLibrary)
[![Platform](https://img.shields.io/cocoapods/p/LitmusCXLibrary.svg?style=flat)](http://cocoapods.org/pods/LitmusCXLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.


## Installation

LitmusCXLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LitmusCXLibrary"
```

## Implementation

To call Litmus Library, do below steps -

## Swift 

### 1. Initiate a conversation in Full Screen

```ruby
import LitmusCXLibrary
```

#### 1.a Open Conversation from App id

```ruby
LitmusCXViewController.fnOpenLitmusFeedback(strBaseUrl, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: tagParameters, isMoreImageBlackElseWhite : isMoreImageBlackElseWhite, isCopyEnabled : isCopyEnabled, isShareEnabled: isShareEnabled, viewController: viewController)
```

#### 1.b Open Conversation from URL

```ruby
LitmusCXViewController.fnOpenWebUrl(strWebUrl, isMoreImageBlackElseWhite : isMoreImageBlackElseWhite, isCopyEnabled : isCopyEnabled, isShareEnabled: isShareEnabled, viewController: viewController)
```

### 2. Initiate a conversation in a Dialog

#### 2.a Open Conversation from App id

```ruby
LitmusCXDialogView.fnOpenLitmusFeedback(strBaseUrl, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: tagParameters, isMoreImageBlackElseWhite : isMoreImageBlackElseWhite, isCopyEnabled : isCopyEnabled, isShareEnabled: isShareEnabled, viewController: viewController)
```

#### 2.b Open Conversation from URL

```ruby
LitmusCXDialogView.fnOpenWebUrl(strWebUrl, isMoreImageBlackElseWhite : isMoreImageBlackElseWhite, isCopyEnabled : isCopyEnabled, isShareEnabled: isShareEnabled, viewController: viewController)
```

## Objective C

```ruby
#import <LitmusCXLibrary/LitmusCXLibrary-Swift.h>
```

#### 1.a Open Conversation from App id

```ruby
[LitmusCXViewController fnOpenLitmusFeedback:strBaseUrl strAppId:strAppId nReminderNumber:nReminderNumber strUserId:strUserId strUserName:strUserName strUserEmail:strUserEmail isAllowMultipleFeedbacks:isAllowMultipleFeedbacks tagParameters:tagParameters isMoreImageBlackElseWhite:isMoreImageBlackElseWhite isCopyEnabled:isCopyEnabled isShareEnabled:isShareEnabled viewController:viewController];
```
#### 1.b Open Conversation from URL

```ruby
[LitmusCXViewController fnOpenWebUrl:strWebUrl isMoreImageBlackElseWhite:isMoreImageBlackElseWhite isCopyEnabled:isCopyEnabled isShareEnabled:isShareEnabled viewController:viewController];
```

### 2. Initiate a conversation in a Dialog

#### 2.a Open Conversation from App id

```ruby
[LitmusCXDialogView fnOpenLitmusFeedback:strBaseUrl strAppId:strAppId nReminderNumber:nReminderNumber strUserId:strUserId strUserName:strUserName strUserEmail:strUserEmail isAllowMultipleFeedbacks:isAllowMultipleFeedbacks tagParameters:tagParameters isMoreImageBlackElseWhite:isMoreImageBlackElseWhite isCopyEnabled:isCopyEnabled isShareEnabled:isShareEnabled viewController:viewController];
```

#### 2.b Open Conversation from URL

```ruby
[LitmusCXDialogView fnOpenWebUrl:strWebUrl isMoreImageBlackElseWhite:isMoreImageBlackElseWhite isCopyEnabled:isCopyEnabled isShareEnabled:isShareEnabled viewController: viewController];
```

#### Where -

- strBaseUrl = Base url to be used to get conversation url (Optional) (Default if not sent is "https://app.litmusworld.com/rateus", For Demo = "https://demo.litmusworld.com", For App india = "https://app-india.litmusworld.com/rateus")
- strAppId = Application feedback Id (To be provided by litmus)
- nReminderNumber = Reminder Number (-1 will call Api every time)
- strUserId = User Id (Required)
- strUserName = User Name (Optional)
- strUserEmail = Email Id of the user (Optional)
- isAllowMultipleFeedbacks = true if multiple feedback is given by same user, false otherwise
- tagParameters = Dictionary with key-value pairs to be sent with above conversation (Optional)
- viewController = UIViewController instance from which it is called
- strWebUrl = URL link to be opened in Webview
- isMoreImageBlackElseWhite = true if you want More Image (Image with 3 dots) as Black in color, false in case of White color
- isCopyEnabled = true if Copy option is enabled in more option, false otherwise
- isShareEnabled = true if Share option is enabled in more option, false otherwise

## Author

Fenil15, fenil.jain@litmusworld.com

## License

LitmusCXLibrary is available under the Apache License, Version 2.0. See the LICENSE file for more info.
